import Vue from 'vue'
import App from './App.vue'
import MainForm from './components/Mainform.vue';
import AutoForm from './components/AutoForm.vue';
import PersonalData from './components/PersonalData.vue';
import ItemsPicker from './components/ItemsPicker.vue';
import CarsPicker from './components/CarsPicker.vue';
import AddItem from './components/AddItem.vue';
import AddCar from './components/AddCar.vue';
import Review from './components/Review.vue';
import ZeroStep from './components/ZeroStep.vue';
import Datepicker from 'vuejs-datepicker';
import SelectDropdown from './components/SelectDropdown.vue';
import VueGoogleAutocomplete from 'vue-google-autocomplete';

Vue.component('app-mainform', MainForm); 
Vue.component('app-autoform', AutoForm); 

Vue.component('personal-data', PersonalData);

Vue.component('items-picker', ItemsPicker);

Vue.component('cars-picker', CarsPicker);

Vue.component('add-item', AddItem);

Vue.component('add-car-form', AddCar);

Vue.component('datepicker', Datepicker);

Vue.component('vue-google-autocomplete', VueGoogleAutocomplete);

Vue.component('custom-select', SelectDropdown);

Vue.component('review-page', Review);

Vue.component('zero-step', ZeroStep);


export const dataBus = new Vue({
	data: {
		userName: '',
		userLastName: '',
		userEmail: '',
		userPhone: '',
		inventory: '',
		//-- data form 
		moveFrom: '',
		moveTo: '',
		moveDate: '',
		dateForSend: ''
	}
});

new Vue({
  el: '#cc-app',
  render: h => h(App)
})
